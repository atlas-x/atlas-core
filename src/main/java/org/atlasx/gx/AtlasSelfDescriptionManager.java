package org.atlasx.gx;

import id.walt.auditor.SimpleVerificationPolicy;
import id.walt.auditor.VerificationPolicy;
import id.walt.auditor.VerificationPolicyResult;
import id.walt.auditor.policies.ExpirationDateAfterPolicy;
import id.walt.auditor.policies.IssuedDateBeforePolicy;
import id.walt.auditor.policies.ValidFromBeforePolicy;
import id.walt.credentials.w3c.VerifiableCredential;
import id.walt.credentials.w3c.builder.W3CCredentialBuilder;
import id.walt.credentials.w3c.templates.VcTemplate;
import id.walt.services.vcstore.VcStoreService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.atlasx.config.AtlasConfigProperties;
import org.atlasx.config.AtlasInitializer;
import org.atlasx.util.AtlasEventPublisher;
import org.atlasx.waltid.AtlasVcTemplateService;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@Slf4j
public class AtlasSelfDescriptionManager {
    private final AtlasEventPublisher eventPublisher;
    private final AtlasConfigProperties configuration;
    private final AtlasInitializer atlasInitializer;
    private final VcStoreService vcStoreService;
    private final AtlasVcTemplateService templateService;
    private final VerificationPolicy signatureValidator;
    private final AtlasSelfDescriptionSubjectBuilder selfDescriptionSubjectBuilder;
    private final AtlasTrustAnchorClient trustAnchorClient;

    public AtlasSelfDescriptionManager(
            AtlasEventPublisher eventPublisher,
            AtlasConfigProperties configuration,
            AtlasInitializer atlasInitializer,
            VcStoreService vcStoreService,
            AtlasVcTemplateService templateService,
            VerificationPolicy signatureValidator,
            AtlasSelfDescriptionSubjectBuilder selfDescriptionSubjectBuilder,
            AtlasTrustAnchorClient trustAnchorClient) {
        this.eventPublisher = eventPublisher;
        this.configuration = configuration;
        this.atlasInitializer = atlasInitializer;
        this.vcStoreService = vcStoreService;
        this.templateService = templateService;
        this.signatureValidator = signatureValidator;
        this.selfDescriptionSubjectBuilder = selfDescriptionSubjectBuilder;
        this.trustAnchorClient = trustAnchorClient;
    }

    public void check() {
        log.debug("check SD");
        if (!atlasInitializer.isInitialized()) {
            log.info("Atlas not initialized - please invoke setup identity api");
            return;
        }
        if (hasStoredSelfDescription()) {
            log.debug("there is a stored SD");
            if (isSelfDescriptionSigned()) {
                log.debug("is signed");
                boolean isSignatureValid = isSelfDescriptionSignatureIsValid();
                boolean isTimeValid = isSelfDescriptionTimesAreValid();
                if (isSignatureValid && isTimeValid) {
                    log.debug("all ok");
                    // do nothing - everything is fine
                    return;
                }
                log.debug("We need to delete SD: SV {}, T {}", isSignatureValid, isTimeValid);
                deleteStoredSd();
            }
//            else {
//                log.debug("fetch issued");
//                fetchIssuedAndSave();
//                // done - will check status next invocation
//                return;
//            }
        } else {
            createCandidateAndSave();
        }
        if(fetchIssuedAndSave()) {
            return;
        }
        log.debug("send for issuing");
        sendSavedForIssuing();
    }

    private void createCandidateAndSave() {
        if (hasStoredSelfDescription()) {
            deleteStoredSd();
        }
        VerifiableCredential empty = getEmpty();
        save(empty);
    }

    private VerifiableCredential sendSavedForIssuing() {
        VerifiableCredential saved = getSaved();
        if(!sendForIssuing(saved)) {
            log.error("Send for issuing failed");
        } else {
            eventPublisher.publish(new AtlasSelfDescriptionChangedEvent(AtlasSelfDescriptionChangedEvent.Type.SEND_FOR_ISSUING));
        }
        return saved;
    }

    private VerifiableCredential getSaved() {
        return vcStoreService.getCredential(configuration.getSelfDescriptionVcId(), configuration.getVcGroup());
    }

    private void deleteStoredSd() {
        try {
            vcStoreService.deleteCredential(configuration.getSelfDescriptionVcId(), configuration.getVcGroup());
            eventPublisher.publish(new AtlasSelfDescriptionChangedEvent(AtlasSelfDescriptionChangedEvent.Type.DELETED));
        } catch (Exception e) {
            log.error("Failed do delete stored credential: {}", e.getMessage());
        }
    }

    private boolean sendForIssuing(VerifiableCredential verifiableCredential) {
        return trustAnchorClient.requestProof(verifiableCredential);
    }

    private void save(VerifiableCredential verifiableCredential) {
        vcStoreService.storeCredential(configuration.getSelfDescriptionVcId(), verifiableCredential, configuration.getVcGroup());
    }

    private boolean fetchIssuedAndSave() {
        log.debug("Fetch self description");
        VerifiableCredential saved = getSaved();
        String savedId = saved.getId();
        log.info("fetch issued and save: {}", savedId);

        Optional<AtlasTrustAnchorClient.FetchProofedResult> optionalProofed =
                trustAnchorClient.fetchProofed(saved, saved); // we need to pass it once as payload and once for authentication
        return optionalProofed.map(result -> {
            // too early. there is still a VC to be signed from the authority
            if (result.code() == 429) {
                log.debug("To early. We need to wait for the signature to be done");
                return true;
            }
            VerifiableCredential proofed = result.credential();
            if (proofed == null) {
                log.debug("No proofed VC available: {}", result.code());
                return false;
            }
            String proofedId = proofed.getId();
            log.debug("ProofedId vs SavedId: {} : {}", proofedId, savedId);

            if (StringUtils.equals(savedId, proofedId)) {
                deleteStoredSd();
                vcStoreService.storeCredential(
                        configuration.getSelfDescriptionVcId(),
                        proofed,
                        configuration.getVcGroup()
                );
                eventPublisher.publish(new AtlasSelfDescriptionChangedEvent(AtlasSelfDescriptionChangedEvent.Type.ISSUED));
                return true;
            }
            return false;
        }).orElse(false);
    }

    private boolean hasStoredSelfDescription() {
        try {
            return getSaved() != null;
        } catch (Exception e) {
            log.error("Got exception while loading SD credential: {}", e.getMessage());
            return false;
        }
    }

    private boolean isSelfDescriptionSigned() {
        try {
            VerifiableCredential credential = getSaved();
            return credential != null && (credential.getSdJwt() != null || credential.getProof() != null);
        } catch (Exception e) {
            log.error("Got exception while getting SD credential signed status: {}", e.getMessage());
        }
        return false;
    }

    private boolean isSelfDescriptionSignatureIsValid() {
        try {
            VerifiableCredential credential = getSaved();
            VerificationPolicyResult policyResult = signatureValidator.verify(Objects.requireNonNull(credential));
            if (!policyResult.isSuccess()) {
                log.error("SD signature not valid: {}", policyResult);
            }
            return policyResult.isSuccess();
        } catch (Exception e) {
            log.error("Got exception while getting SD credential signed status: {}", e.getMessage());
        }
        return false;
    }

    private boolean isSelfDescriptionTimesAreValid() {
        try {
            VerifiableCredential credential = getSaved();
            List<SimpleVerificationPolicy> policies = List.of(new IssuedDateBeforePolicy(),
                    new ValidFromBeforePolicy(),
                    new ExpirationDateAfterPolicy());

            return policies.stream().filter(p -> !p.verify(Objects.requireNonNull(credential)).isSuccess()).map(p -> {
                log.error("SD times validation failed: {}", p.verify(credential));
                return false;
            }).findFirst().orElse(true);
        } catch (Exception e) {
            log.error("Got exception while getting SD credential signed status: {}", e.getMessage());
        }
        return false;
    }

    public VerifiableCredential createByTemplateString(String templateString) {
        String name = configuration.getSelfDescriptionTemplate();
        VerifiableCredential template = VerifiableCredential.Companion.fromJson(templateString);
        templateService.invalidateCache();
        templateService.unregisterTemplate(name);
        templateService.register(name, template);

        vcStoreService.deleteCredential(configuration.getSelfDescriptionVcId(), configuration.getVcGroup());
        createCandidateAndSave();
        return sendSavedForIssuing();
    }

    public VerifiableCredential getEmpty() {
        String id = UUID.randomUUID().toString();
        String credentialId = configuration.getServiceBaseAddress() + "/gx/sd/" + id;
        String subjectId = me();
        VcTemplate sdCredential = templateService.getTemplate(
                configuration.getSelfDescriptionTemplate(),
                true,
                configuration.getTemplatesFolder()
        );

        W3CCredentialBuilder w3CCredentialBuilder = W3CCredentialBuilder.Companion
                .fromPartial(Objects.requireNonNull(sdCredential.getTemplate()));
        w3CCredentialBuilder.setSubjectId(subjectId);
        w3CCredentialBuilder.setId(credentialId);

        selfDescriptionSubjectBuilder.build(w3CCredentialBuilder::setFromJson);

        return w3CCredentialBuilder.build();
    }


    private String me() {
        return atlasInitializer.getIssuerDid();
    }
}
