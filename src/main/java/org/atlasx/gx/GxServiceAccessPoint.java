package org.atlasx.gx;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GxServiceAccessPoint {
    @JsonProperty("gx:host")
    private String host;
    @JsonProperty("gx:protocol")
    private String protocol;
    @JsonProperty("gx:port")
    private String port;
    @JsonProperty("gx:openAPI")
    private String openAPI;
}
