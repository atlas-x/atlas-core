package org.atlasx.gx;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.walt.credentials.w3c.VerifiableCredential;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.hc.core5.http.ContentType;
import org.apache.hc.core5.http.HttpStatus;
import org.apache.hc.core5.http.Method;
import org.apache.hc.core5.http.io.support.ClassicRequestBuilder;
import org.atlasx.AtlasCommunicator;
import org.atlasx.catalogue.AtlasCatalogue;
import org.atlasx.util.CustomHttpClientResponseHandler;

import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Slf4j
public class AtlasTrustAnchorClient {
    private final AtlasCatalogue catalogue;
    private final AtlasCommunicator communicator;
    private final ObjectMapper objectMapper;

    public AtlasTrustAnchorClient(
            AtlasCatalogue catalogue,
            AtlasCommunicator communicator,
            ObjectMapper objectMapper
    ) {
        this.catalogue = catalogue;
        this.communicator = communicator;
        this.objectMapper = objectMapper;
    }

    public boolean requestProof(VerifiableCredential credential) {
        return requestProof(credential, credential);
    }

    public boolean requestProof(VerifiableCredential credential, VerifiableCredential auth) {
        Optional<TrustAnchor> trustAnchor = findTrustAnchor(credential);
        return trustAnchor.map(ta -> {
            String body = credential.encode();
            URI uri = URI.create(ta.webAddress + "/trust");
            ClassicRequestBuilder requestBuilder = communicator.requestBuilder(Method.POST, ContentType.TEXT_PLAIN, uri);
            requestBuilder.setEntity(body);
            try {
                log.info("Send vc for proofing: {}", body);
                int code = communicator.exchange(requestBuilder).code();
                return (code >= HttpStatus.SC_OK && code < HttpStatus.SC_MULTIPLE_CHOICES);
            } catch (Exception e) {
                log.info("Exception while communicating", e);
                return false;
            }
        }).orElseGet(() -> {
            log.info("No trust anchor found");
            return false;
        });
    }

    public Optional<VerifiableCredential> fetchProofed(VerifiableCredential credential) {
        return fetchProofed(credential, null).map(e -> e.credential);
    }

    public Optional<FetchProofedResult> fetchProofed(VerifiableCredential credential, VerifiableCredential auth) {
        Optional<TrustAnchor> trustAnchor = findTrustAnchor(credential);
        return trustAnchor.map(ta -> {
            URI uri = URI.create(ta.webAddress + "/trust?vcId=" + credential.getId());
            log.info("Fetch proofed: {}", uri.toString());
            ClassicRequestBuilder requestBuilder = communicator.requestBuilder(Method.GET, ContentType.TEXT_PLAIN, uri);

            try {
                CustomHttpClientResponseHandler.Response exchanged = communicator.exchange(requestBuilder);
                int statusCode = exchanged.code();
                if ((statusCode >= HttpStatus.SC_OK && statusCode < HttpStatus.SC_MULTIPLE_CHOICES) && exchanged.e() == null) {
                    String body = new String(exchanged.data(), StandardCharsets.UTF_8);
                    return new FetchProofedResult(
                            VerifiableCredential.Companion.fromString(body),
                            statusCode
                    );
                }
                return new FetchProofedResult(null, statusCode);
            } catch (Exception e) {
                String message = e.getMessage();
                log.info("Exception while communicating: {}", message);
                String sub = (message + "---").substring(0, 3);
                if(sub.matches("^\\d+$")) {
                    return new FetchProofedResult(
                            null, Integer.parseInt(sub, 10)
                    );
                }
                return null;
            }
        });
    }

    private Optional<TrustAnchor> findTrustAnchor(VerifiableCredential credential) {
        //TODO: we should store the selected TrustAnchor for each VC, otherwise we might post and get from different SOs
        List<VerifiableCredential> serviceOfferingsTrustAnchor = catalogue.find("ServiceOfferingTrustAnchor");
        if (serviceOfferingsTrustAnchor.isEmpty()) {
            return Optional.empty();
        }

        Set<String> credentialType = Set.copyOf(credential.getType());

        return serviceOfferingsTrustAnchor
                .stream()
                .map(this::map)
                .filter(s -> s.provableTypes.stream().anyMatch(credentialType::contains))
                .findFirst();
    }

    @SneakyThrows
    private TrustAnchor map(VerifiableCredential vc) {
        GxServiceOffering subject = objectMapper.readValue(vc.getCredentialSubject().toJson(), GxServiceOffering.class);

        String endpoint = ServiceOfferingUtils.resolveEndpointAddress(subject)
                .orElseThrow( () -> new IllegalStateException("ServiceOffering does not provide endpoint"));
        List<String> provableTypes = ServiceOfferingUtils.resolveProvableTypes(subject);

        return new TrustAnchor(
                endpoint,
                new LinkedHashSet<>(provableTypes)
        );
    }

    private record TrustAnchor(
            String webAddress,
            Set<String> provableTypes
    ) {}

    public record FetchProofedResult(
            VerifiableCredential credential,
            int code
    ) {}
}
