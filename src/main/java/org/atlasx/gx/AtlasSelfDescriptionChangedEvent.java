package org.atlasx.gx;

public record AtlasSelfDescriptionChangedEvent(Type sendForIssuing) {
    public enum Type {
        DELETED,
        SEND_FOR_ISSUING,
        ISSUED
    }
}
