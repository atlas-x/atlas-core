package org.atlasx.gx;

import org.atlasx.config.AtlasConfigProperties;

public class SelfDescriptionUtils {
    public static String getSDAddress(AtlasConfigProperties configuration) {
        return "https://" + configuration.getDomain() + "/gx/" + configuration.getSelfDescriptionVcId();
    }
}
