package org.atlasx.gx;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.walt.credentials.w3c.VerifiableCredential;
import id.walt.credentials.w3c.builder.W3CCredentialBuilder;
import id.walt.credentials.w3c.templates.VcTemplate;
import id.walt.signatory.ProofType;
import id.walt.signatory.Signatory;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.atlasx.config.AtlasConfigProperties;
import org.atlasx.config.AtlasInitializer;
import org.atlasx.config.ProofConfigBuilder;
import org.atlasx.waltid.AtlasVcTemplateService;

import java.net.URI;
import java.time.Instant;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j

public class AtlasServiceOfferingManager {
    private final AtlasConfigProperties configuration;
    private final AtlasInitializer atlasInitializer;
    private final Signatory signatoryService;
    private final AtlasVcTemplateService templateService;
    private final OpenApiProvider openApiProvider;
    private final ObjectMapper mapper;

    public AtlasServiceOfferingManager(
            AtlasConfigProperties configuration,
            AtlasInitializer atlasInitializer,
            Signatory signatoryService,
            AtlasVcTemplateService templateService,
            OpenApiProvider openApiProvider,
            ObjectMapper mapper) {
        this.configuration = configuration;
        this.atlasInitializer = atlasInitializer;
        this.signatoryService = signatoryService;
        this.templateService = templateService;
        this.openApiProvider = openApiProvider;
        this.mapper = mapper;
    }

    //    @PostConstruct
    public Map<String, VerifiableCredential> getAll() {
        try {
            Map<String, VcTemplate> templates = templateService
                    .listTemplates(configuration.getTemplatesFolder())
                    .stream()
                    .map(VcTemplate::getName)
                    .filter(n -> n.toLowerCase().startsWith("ServiceOffering".toLowerCase()))
                    .collect(Collectors.toMap(n -> n, this::getTemplate));

            return templates.keySet().stream()
                    .filter(name -> templates.get(name).getTemplate() != null)
                    .collect(Collectors.toMap(name -> name, name -> prepare(templates.get(name))));
        } catch (Exception e) {
            log.info("Failed to load local offerings: {}", e.getMessage());
            return new HashMap<>();
        }
    }

    public Map<String, Object> listJsonPresentation(boolean validOnly) {
        Map<String, VerifiableCredential> all = getAll();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'kk:mm:ss'Z'");

        return all.entrySet()
                .stream()
                .filter(it ->
                        validOnly
                                ? LocalDate.parse(it.getValue().getExpirationDate(), formatter).isAfter(LocalDate.now())
                                : true)
                .map(e -> new Touple(e.getKey(), this.mapCatching(e.getValue().encode())))
                .filter(t -> t.value().isPresent())
                .collect(Collectors.toMap(t -> t.key, t -> t.value.get()));
    }

    public String getCredentialSubject(String name) {
        return configuration.getServiceBaseAddress() +
                configuration.getApiDocsBasePath() +
                "/" +
                name;
    }

    public String getWebAddress(String name) {
        return configuration.getServiceBaseAddress();
    }

    private VcTemplate getTemplate(String name) {
        return templateService.getTemplate(name, true, configuration.getTemplatesFolder());
    }

    VerifiableCredential prepare(VcTemplate template) {
        String name = template.getName();
        String issuer = atlasInitializer.getIssuerDid();
        String issuerSDAddress = SelfDescriptionUtils.getSDAddress(configuration);
        String credentialId = configuration.getServiceBaseAddress() + "/gx/so/" + name;
        GxServiceOffering offering = GxServiceOffering.builder()
                .providedBy(issuerSDAddress)
                .maintainedBy(issuerSDAddress)
                .tenantOwnedBy(issuerSDAddress)
                .serviceAccessPoint(List.of(getServiceAccessPoint(name)))
                .webAddress(getWebAddress(name))
                .api(getApiDefinition(name).orElse(null))
                .build();

        String subject = getCredentialSubject(name);
        createProofConfig(issuer);
        W3CCredentialBuilder w3CCredentialBuilder = W3CCredentialBuilder.Companion.fromPartial(Objects.requireNonNull(template.getTemplate()));
        w3CCredentialBuilder.setSubjectId(subject);
        Set<String> typesWithMarker = new LinkedHashSet<>(w3CCredentialBuilder.getType());
        typesWithMarker.add(name);
        w3CCredentialBuilder.setFromJson(this.buildJsonPatch(offering, typesWithMarker));

        String vc = signatoryService.issue(
                w3CCredentialBuilder,
                createProofConfig(issuer).withCredentialId(credentialId).withSubjectDid(subject).withProofType(ProofType.LD_PROOF).build(),
                null,
                false
        );
        return VerifiableCredential.Companion.fromString(vc);
    }

    @SneakyThrows
    private GxServiceAccessPoint getServiceAccessPoint(String name) {
        URI baseAddress = new URI(configuration.getServiceBaseAddress());
        return GxServiceAccessPoint.builder()
                .host(baseAddress.getHost())
                .protocol(baseAddress.getScheme())
                .port(baseAddress.getPort() != -1 ? "" + baseAddress.getPort() : null)
                .openAPI(getCredentialSubject(name))
                .build();
    }

    @SneakyThrows
    private String buildJsonPatch(GxServiceOffering subject, Set<String> typesWithMarker) {
        Map<String, Object> patch = new HashMap<>();
        patch.put("credentialSubject", subject);
        patch.put("type", typesWithMarker);
        return mapper.writeValueAsString(patch);
    }

    private Optional<Object> getApiDefinition(String name) {
        try {
            String jsonDefinition = openApiProvider.getFor(name);
            return mapCatching(jsonDefinition);
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    private Optional<Object> mapCatching(String s) {
        try {
            return Optional.ofNullable(mapper.readValue(s, Object.class));
        } catch (JsonProcessingException e) {
            return Optional.empty();
        }
    }

    private ProofConfigBuilder createProofConfig(String issuerDid) {
        Instant expiration = Instant.now().plus(30, ChronoUnit.DAYS);
        return ProofConfigBuilder.create(issuerDid)
                .withCredentialId("urn:uuid:" + UUID.randomUUID())
                .withSubjectDid(issuerDid)
                .withProofType(ProofType.JWT)
                .withExpirationDate(expiration);
    }

    private record Touple(String key, Optional<Object> value) {
    }

}
