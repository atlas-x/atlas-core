package org.atlasx.gx;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GxServiceOffering {

    //ServiceOffering fields
    @JsonProperty("gx:providedBy")
    private String providedBy;

    @JsonProperty("gx:dataProtectionRegime")
    private List<String> dataProtectionRegime;

    @JsonProperty("gx:description")
    private String description;

    @JsonProperty("gx:name")
    private String name;

    //ServiceInstance fields
    @JsonProperty("gx:maintainedBy")
    private String maintainedBy;
    @JsonProperty("gx:tenantOwnedBy")
    private String tenantOwnedBy;

    @JsonProperty("gx:serviceAccessPoint")
    private List<GxServiceAccessPoint> serviceAccessPoint;

    @JsonProperty("gx:provableTypes")
    private List<String> provableTypes;

    @JsonProperty("gx-service-offering:webAddress")
    @Deprecated(forRemoval = true)
    private String webAddress;

    @JsonProperty("gx-service-offering:provableTypes")
    @Deprecated(forRemoval = true)
    private List<String> deprecatedProvableTypes;

    @JsonProperty("gx-service-offering:api")
    @Deprecated(forRemoval = true)
    private Object api;
}
