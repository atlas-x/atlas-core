package org.atlasx.gx;

import org.apache.commons.lang3.StringUtils;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class ServiceOfferingUtils {

    public static Optional<String> resolveEndpointAddress(GxServiceOffering serviceOfferingSubject) {
        if (serviceOfferingSubject == null) {
            return Optional.empty();
        }
        if (serviceOfferingSubject.getServiceAccessPoint() != null && !serviceOfferingSubject.getServiceAccessPoint().isEmpty()) {
            GxServiceAccessPoint serviceAccessPoint = serviceOfferingSubject.getServiceAccessPoint().getFirst();
            String webAddress = serviceAccessPoint.getProtocol() + "://" + serviceAccessPoint.getHost();
            if (StringUtils.isNotBlank(serviceAccessPoint.getPort())) {
                webAddress += ":" + serviceAccessPoint.getPort();
            }
            return Optional.of(webAddress);
        }
        if (StringUtils.isNotBlank(serviceOfferingSubject.getWebAddress())) {
            return Optional.of(serviceOfferingSubject.getWebAddress());
        }

        return Optional.empty();
    }

    public static List<String> resolveProvableTypes(GxServiceOffering serviceOfferingSubject) {
        if (serviceOfferingSubject == null) {
            return Collections.emptyList();
        }

        List<String> provableTypes = serviceOfferingSubject.getProvableTypes();
        if(provableTypes != null && !provableTypes.isEmpty()){
            return provableTypes;
        }

        List<String> deprecatedProvableTypes = serviceOfferingSubject.getDeprecatedProvableTypes();
        if(deprecatedProvableTypes != null && !deprecatedProvableTypes.isEmpty()){
            return deprecatedProvableTypes;
        }

        return Collections.emptyList();
    }
}
