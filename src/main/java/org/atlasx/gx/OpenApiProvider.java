package org.atlasx.gx;

public interface OpenApiProvider {
    String getFor(String group);
}
