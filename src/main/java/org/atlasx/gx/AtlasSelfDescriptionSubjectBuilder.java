package org.atlasx.gx;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public class AtlasSelfDescriptionSubjectBuilder {
    private final ObjectMapper mapper;

    private List<String> role;

    public AtlasSelfDescriptionSubjectBuilder(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    @SneakyThrows
    public void build(Consumer<String> o) {
        if (mapper == null) {
            return;
        }
        Map<String, Object> data = new HashMap<>();

        if (role != null) {
            data.put("role", role);
        }

        Map<String, Object> body = new HashMap<>();
        body.put("credentialSubject", data);
        String json = mapper.writeValueAsString(body);
        o.accept(json);
    }

    public AtlasSelfDescriptionSubjectBuilder withRole(String role) {
        if (this.role == null) {
            this.role = new ArrayList<>();
        }
        this.role.add(role);
        return this;
    }

    public AtlasSelfDescriptionSubjectBuilder resetRoles() {
        role = null;
        return this;
    }
}
