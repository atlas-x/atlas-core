package org.atlasx;

public record AtlasInterceptorEvent(Type type, String method, String servletPath, String token) {

    public enum Type {
        MISSING_AUTHORIZATION_HEADER,
        AUTHORIZATION_SUCCESS,
        AUTHORIZATION_FAILURE,
        AUTHORIZATION_EXCEPTION
    }
}
