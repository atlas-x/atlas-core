package org.atlasx.config;

import id.walt.crypto.LdSignatureType;
import id.walt.model.credential.status.CredentialStatus;
import id.walt.signatory.Ecosystem;
import id.walt.signatory.ProofConfig;
import id.walt.signatory.ProofType;

import java.time.Instant;

public class ProofConfigBuilder {

    private String issuerDid;
    private String subjectDid = null;
    private String verifierDid = null;
    private String issuerVerificationMethod = null; // DID URL that defines key ID; if null the issuers' default key is used
    private ProofType proofType = ProofType.JWT;
    private String domain = null;
    private String nonce = null;
    private String proofPurpose = null;
    private String credentialId = null;
    private Instant issueDate = null; // issue date from json-input or current system time if null
    private Instant validDate = null; // valid date from json-input or current system time if null
    private Instant expirationDate = null;
    private String dataProviderIdentifier = null; // may be used for mapping data-sets from a custom data-provider
    private LdSignatureType ldSignatureType = null;
    private String creator;
    private Ecosystem ecosystem = Ecosystem.DEFAULT;

    private CredentialStatus.Types statusType;
    private String statusPurpose = "revocation";
    private String credentialsEndpoint = null;

    public static ProofConfigBuilder create(String issuerDid) {
        return new ProofConfigBuilder(issuerDid);
    }

    public ProofConfig build() {
        return new ProofConfig(
                issuerDid = issuerDid,
                subjectDid = subjectDid,
                verifierDid = verifierDid,
                issuerVerificationMethod = issuerVerificationMethod,
                proofType = proofType,
                domain = domain,
                nonce = nonce,
                proofPurpose = proofPurpose,
                credentialId = credentialId,
                issueDate = issueDate,
                validDate = validDate,
                expirationDate = expirationDate,
                dataProviderIdentifier = dataProviderIdentifier,
                ldSignatureType = ldSignatureType,
                creator = creator,
                ecosystem = ecosystem,
                statusType = statusType,
                statusPurpose = statusPurpose,
                credentialsEndpoint = credentialsEndpoint,
                null
        );
    }

    private ProofConfigBuilder(String issuerDid) {
        this.issuerDid = issuerDid;
        this.creator = issuerDid;
    }

    public ProofConfigBuilder withSubjectDid(String subjectDid) {
        this.subjectDid = subjectDid;
        return this;
    }

    public ProofConfigBuilder withVerifierDid(String verifierDid) {
        this.verifierDid = verifierDid;
        return this;
    }

    public ProofConfigBuilder withIssuerVerificationMethod(String issuerVerificationMethod) {
        this.issuerVerificationMethod = issuerVerificationMethod;
        return this;
    }

    public ProofConfigBuilder withProofType(ProofType proofType) {
        this.proofType = proofType;
        return this;
    }

    public ProofConfigBuilder withDomain(String domain) {
        this.domain = domain;
        return this;
    }

    public ProofConfigBuilder withNonce(String nonce) {
        this.nonce = nonce;
        return this;
    }

    public ProofConfigBuilder withProofPurpose(String proofPurpose) {
        this.proofPurpose = proofPurpose;
        return this;
    }

    public ProofConfigBuilder withCredentialId(String credentialId) {
        this.credentialId = credentialId;
        return this;
    }

    public ProofConfigBuilder withIssueDate(Instant issueDate) {
        this.issueDate = issueDate;
        return this;
    }

    public ProofConfigBuilder withValidDate(Instant validDate) {
        this.validDate = validDate;
        return this;
    }

    public ProofConfigBuilder withExpirationDate(Instant expirationDate) {
        this.expirationDate = expirationDate;
        return this;
    }

    public ProofConfigBuilder withDataProviderIdentifier(String dataProviderIdentifier) {
        this.dataProviderIdentifier = dataProviderIdentifier;
        return this;
    }

    public ProofConfigBuilder withLdSignatureType(LdSignatureType ldSignatureType) {
        this.ldSignatureType = ldSignatureType;
        return this;
    }

    public ProofConfigBuilder withCreator(String creator) {
        this.creator = creator;
        return this;
    }

    public ProofConfigBuilder withEcosystem(Ecosystem ecosystem) {
        this.ecosystem = ecosystem;
        return this;
    }

}
