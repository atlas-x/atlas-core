package org.atlasx.config;

public interface AtlasJwtConfigBuilder {
    void configure(AtlasJwtAuthorizationConfigurer configurer);
}
