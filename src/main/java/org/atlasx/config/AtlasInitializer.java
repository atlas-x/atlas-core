package org.atlasx.config;

import com.apicatalog.jsonld.document.Document;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import foundation.identity.jsonld.ConfigurableDocumentLoader;
import foundation.identity.jsonld.JsonLDObject;
import id.walt.crypto.Key;
import id.walt.crypto.KeyAlgorithm;
import id.walt.crypto.KeyId;
import id.walt.model.Did;
import id.walt.model.DidMethod;
import id.walt.model.VerificationMethod;
import id.walt.services.did.DidOptions;
import id.walt.services.did.DidService;
import id.walt.services.key.KeyService;
import info.weboftrust.ldsignatures.jsonld.LDSecurityContexts;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.net.URI;
import java.util.Objects;

@Slf4j
public class AtlasInitializer {
    private final AtlasConfigProperties configuration;
    private final KeyService keyService;
    private final DidService didService;
    private final AtlasDidOptionsFactory didOptionsFactory;

    @Getter
    private boolean initialized = false;
    @Getter
    private String issuerDid;

    public AtlasInitializer(
            AtlasConfigProperties configuration,
            KeyService keyService,
            DidService didService,
            AtlasDidOptionsFactory didOptionsFactory
    ) {
        this.configuration = configuration;
        this.keyService = keyService;
        this.didService = didService;
        this.didOptionsFactory = didOptionsFactory;
        this.configureContextCache();
    }

    public void configureContextCache() {
        Cache<URI, Document> cache = Caffeine
                .newBuilder()
                .expireAfterWrite(configuration.getContextDocumentCacheDuration())
                .maximumSize(1000).build();
        configureDocumentLoader((ConfigurableDocumentLoader) JsonLDObject.DEFAULT_DOCUMENT_LOADER, cache);
        configureDocumentLoader((ConfigurableDocumentLoader) LDSecurityContexts.DOCUMENT_LOADER, cache);
    }

    private void configureDocumentLoader(ConfigurableDocumentLoader loader, Cache<URI, Document> cache) {
        loader.setEnableHttps(true);
        loader.setRemoteCache(cache);
    }

    private void setup(boolean createIfMissing) {
        final String keyAlias = configuration.getSignatoryKeyAlias();
        KeyId serviceKeyId = null;

        try {
            Key serviceKey = keyService.load(keyAlias);
            serviceKeyId = serviceKey.getKeyId();
        } catch (Exception e) {
            if (createIfMissing) {
                serviceKeyId = keyService.generate(KeyAlgorithm.EdDSA_Ed25519);
                keyService.load(serviceKeyId.getId());
                keyService.addAlias(serviceKeyId, keyAlias);

                DidMethod method = configuration.getDefaultDidMethod();
                DidOptions options = didOptionsFactory.getOptions(serviceKeyId);

                didService.create(method, serviceKeyId.getId(), options);
            }
        }

        if (serviceKeyId == null) {
            return;
        }

        String keyId = serviceKeyId.getId();
        Did didDoc = didService
                .listDids()
                .stream()
                .filter(name -> didNameMatches(name, keyId))
                .findFirst()
                .map(didService::load)
                .orElse(null);

        String issuerDid = didDoc != null ? didDoc.getId() : null;
        log.info("DID {} Document loaded: {}", configuration.getDefaultDidMethod(), issuerDid);
        initialized = true;
        this.issuerDid = issuerDid;
    }

    public Did getIssuerDidDocument() {
        return didService.load(issuerDid);
    }

    public VerificationMethod getIssuerVerificationMethod() {
        Did didDoc = getIssuerDidDocument();
        return Objects.requireNonNull(didDoc.getVerificationMethod()).stream().findFirst().orElseThrow();
    }

    public synchronized void init() {
        if (initialized) {
            return;
        }
        setup(configuration.isAutoCreateSignatoryDid());
    }

    private boolean didNameMatches(String name, String keyId) {
        return name.startsWith("did:" + configuration.getDefaultDidMethod()+":") && name.endsWith(keyId);
    }
}
