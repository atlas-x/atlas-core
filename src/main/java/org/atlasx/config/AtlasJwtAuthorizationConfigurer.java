package org.atlasx.config;

import id.walt.auditor.VerificationPolicy;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class AtlasJwtAuthorizationConfigurer {
    private final Map<String, Method> paths;
    private final AtlasJwtConfigBuilder configBuilder;
    private final Map<String, Map<Method, List<VerificationPolicy>>> policies;

    public AtlasJwtAuthorizationConfigurer(AtlasJwtConfigBuilder configBuilder) {
        this.configBuilder = configBuilder;
        paths = new HashMap<>();
        policies = new HashMap<>();
    }

    public void init() {
        configBuilder.configure(this);
    }

    public boolean authorizationRequired(String path, String method) {
        return paths.keySet()
                .stream()
                .filter(path::matches)
                .findFirst()
                .map(paths::get)
                .map(m -> m.is(method))
                .orElse(false);
    }

    public List<VerificationPolicy> policies(String path, String method) {
        Method m = Method.valueOf(method);
        List<VerificationPolicy> verificationPolicies = policies.keySet()
                .stream()
                .filter(path::matches)
                .map(policies::get)
                .filter(it -> it.containsKey(m) || it.containsKey(Method.ALL))
                .map(it -> Optional.ofNullable(it.get(m)).orElse(it.get(Method.ALL)))
                .findFirst()
                .orElseGet(List::of);
        return verificationPolicies;
    }

    public static class AtlasJwtAuthorizationConfigurerBuilder {
        private final AtlasJwtAuthorizationConfigurer parent;
        private final String path;
        public AtlasJwtAuthorizationConfigurerBuilder(AtlasJwtAuthorizationConfigurer parent, String path) {
            this.parent = parent;
            this.path = path;
        }

        public AtlasJwtAuthorizationConfigurer all() {
            parent.paths.put(path, Method.ALL);
            return parent;
        }

        public AtlasJwtAuthorizationConfigurer all(List<VerificationPolicy> policyList) {
            Map<Method, List<VerificationPolicy>> target = parent.policies.computeIfAbsent(path, (a) -> new HashMap<>());
            target.put(Method.ALL, policyList);
            return all();
        }

        public AtlasJwtAuthorizationConfigurer get() {
            parent.paths.put(path, Method.GET);
            return parent;
        }

        public AtlasJwtAuthorizationConfigurer get(List<VerificationPolicy> policyList) {
            Map<Method, List<VerificationPolicy>> target = parent.policies.computeIfAbsent(path, (a) -> new HashMap<>());
            target.put(Method.GET, policyList);
            return get();
        }

        public AtlasJwtAuthorizationConfigurer post(List<VerificationPolicy> policyList) {
            Map<Method, List<VerificationPolicy>> target = parent.policies.computeIfAbsent(path, (a) -> new HashMap<>());
            target.put(Method.POST, policyList);
            return post();
        }

        public AtlasJwtAuthorizationConfigurer post() {
            parent.paths.put(path, Method.POST);
            return parent;
        }

        public AtlasJwtAuthorizationConfigurer put() {
            parent.paths.put(path, Method.PUT);
            return parent;
        }

        public AtlasJwtAuthorizationConfigurer patch() {
            parent.paths.put(path, Method.PATCH);
            return parent;
        }

        public AtlasJwtAuthorizationConfigurer delete() {
            parent.paths.put(path, Method.DELETE);
            return parent;
        }
    }

    public AtlasJwtAuthorizationConfigurerBuilder configure(String path) {
        return new AtlasJwtAuthorizationConfigurerBuilder(this, path);
    }

    interface Fun {
        boolean is(String s);
    }


    final static Fun get = "GET"::equalsIgnoreCase;
    final static Fun post = "POST"::equalsIgnoreCase;
    final static Fun put = "PUT"::equalsIgnoreCase;
    final static Fun patch = "PATCH"::equalsIgnoreCase;
    final static Fun delete = "DELETE"::equalsIgnoreCase;
    enum Method {

        ALL((String s) -> get.is(s) || post.is(s) || put.is(s) || patch.is(s) || delete.is(s)),
        GET(get),
        POST(post),
        PUT(put),
        PATCH(patch),
        DELETE(delete);

        private final Fun o;

        Method(Fun o) {
            this.o = o;
        }
        boolean is(String s) {
            return o.is(s);
        }
    }
}
