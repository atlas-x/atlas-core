package org.atlasx.config;

import id.walt.auditor.Auditor;
import id.walt.custodian.Custodian;
import id.walt.servicematrix.BaseService;
import id.walt.servicematrix.ServiceRegistry;
import id.walt.services.context.ContextManager;
import id.walt.services.crypto.CryptoService;
import id.walt.services.hkvstore.HKVStoreService;
import id.walt.services.jwt.JwtService;
import id.walt.services.key.KeyService;
import id.walt.services.keystore.KeyStoreService;
import id.walt.services.vc.JsonLdCredentialService;
import id.walt.services.vc.JwtCredentialService;
import id.walt.services.vcstore.VcStoreService;
import kotlin.jvm.JvmClassMappingKt;
import org.atlasx.util.AtlasEventPublisher;

public class AtlasContextInitializer {
    private final AtlasInitializer atlasInitializer;

    private final JsonLdCredentialService jsonLdCredentialService;
    private final JwtCredentialService jwtCredentialService;
    private final CryptoService cryptoService;
    private final KeyStoreService keyStoreService;
    private final KeyService keyService;
    private final JwtService jwtService;
    private final VcStoreService vcStoreService;
    private final HKVStoreService hKVStoreService;
    private final Custodian custodian;
    private final Auditor auditor;
    private final ContextManager contextManager;

    public AtlasContextInitializer(
            AtlasInitializer atlasInitializer,
            JsonLdCredentialService jsonLdCredentialService,
            JwtCredentialService jwtCredentialService,
            CryptoService cryptoService,
            KeyStoreService keyStoreService,
            KeyService keyService,
            JwtService jwtService,
            VcStoreService vcStoreService,
            HKVStoreService hKVStoreService,
            Custodian custodian,
            Auditor auditor,
            ContextManager contextManager
    ) {
        this.atlasInitializer = atlasInitializer;
        this.jsonLdCredentialService = jsonLdCredentialService;
        this.jwtCredentialService = jwtCredentialService;
        this.cryptoService = cryptoService;
        this.keyStoreService = keyStoreService;
        this.keyService = keyService;
        this.jwtService = jwtService;
        this.vcStoreService = vcStoreService;
        this.hKVStoreService = hKVStoreService;
        this.custodian = custodian;
        this.auditor = auditor;
        this.contextManager = contextManager;
    }

    public void init() {
        registerService(jsonLdCredentialService, JsonLdCredentialService.class);
        registerService(jwtCredentialService, JwtCredentialService.class);
        registerService(cryptoService, CryptoService.class);
        registerService(keyStoreService, KeyStoreService.class);
        registerService(keyService, KeyService.class);
        registerService(jwtService, JwtService.class);
        registerService(vcStoreService, VcStoreService.class);
        registerService(hKVStoreService, HKVStoreService.class);
        registerService(custodian, Custodian.class);
        registerService(auditor, Auditor.class);
        registerService(contextManager, ContextManager.class);

        atlasInitializer.init();


//        scheduledConfiguration.init();
    }

    private void registerService(BaseService obj, Class<? extends BaseService> type) {
        ServiceRegistry.INSTANCE.registerService(obj, JvmClassMappingKt.getKotlinClass(type));
    }
}
