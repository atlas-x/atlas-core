package org.atlasx.config;

import id.walt.crypto.KeyId;
import id.walt.services.did.DidOptions;

import java.util.List;

public class AtlasDidOptionsFactory {
    private final AtlasConfigProperties configuration;
    private final List<AtlasDidOptionProvider> providers;

    public AtlasDidOptionsFactory(
            AtlasConfigProperties configuration,
            List<AtlasDidOptionProvider> providers
    ) {
        this.configuration = configuration;
        this.providers = providers;
    }

    public DidOptions getOptions(KeyId serviceKeyId) {
        return providers
                .stream()
                .filter(p -> p.method() == configuration.getDefaultDidMethod())
                .findFirst()
                .map(p -> p.options(serviceKeyId))
                .orElseThrow();
    }
}
