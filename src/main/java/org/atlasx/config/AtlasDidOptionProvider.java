package org.atlasx.config;

import id.walt.crypto.KeyId;
import id.walt.model.DidMethod;
import id.walt.services.did.DidOptions;

public interface AtlasDidOptionProvider {
    DidMethod method();
    DidOptions options(KeyId keyId);
}
