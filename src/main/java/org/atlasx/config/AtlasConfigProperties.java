package org.atlasx.config;

import id.walt.model.DidMethod;
import lombok.Getter;
import lombok.Setter;

import java.time.Duration;

@Getter
@Setter
public class AtlasConfigProperties {
    /**
     * The group which within your VCs should be stored
     */
    private String vcGroup = "default";

    /**
     * the alias of your private key.
     * This key will be used to create your did and sign all your VC/VPs
     */
    private String signatoryKeyAlias = "signatory";

    /**
     * Your did-web domain. This domain needs to point to your service instance
     */
    private String domain = "atlas.cartrust.com";

    /**
     * The domain suffix of your service. Should not be changed
     */
    private String didWebPath = "gx/did";

    /**
     * Parameter of underlying waltid-ssikit configuration
     */
    private String nonce = "todo";

    /**
     * The location where your templates are stored.
     */
    private String templatesFolder = "/vc-templates";

    /**
     * Template for GxSelfDescription.
     * Either do not modify or provide new template in your templates folder.
     */
    private String selfDescriptionTemplate = "GaiaxCredentialSD";

    /**
     * Identifier where to store the SelfDescriptionVC
     */
    private String selfDescriptionVcId = "sd";

    /**
     * Identifier where to store the LegalRegistrationNumberVc
     */
    private String legalRegistrationNumberVcId = "legalRegistrationNumber";

    /**
     * Your web address of the service. This address is used for ServiceOfferings. It should match your domain property as https address.
     */
    private String serviceBaseAddress = "https://example.com";

    /**
     * Base path of open-api docs endpoint.
     * No need to change if you don't want to reconfigure the OpenAPI
     */
    private String apiDocsBasePath = "/v3/api-docs";

    /**
     * Set to true, if you want to sign your SelfDescription.
     * Only a root authority service should set this to true
     */
    private boolean selfSignSelfDescription;

    /**
     * Set to true, to unsigned SelfDescription
     */
    private boolean returnEmptySelfDescription = true;

    /**
     * Set to true, to start scheduled process to send SelfDescription to authorityVcAddress.
     * Otherwise, the process can be started via SetupController.
     * To use this feature, you need to enable scheduling for your project.
     * This can be done by adding `@EnableScheduling` annotation.
     */
    private boolean autostartSelfDescriptionSchedule = false;

    /**
     * Set this to true, to create key and did document if not present. Otherwise, you need to invoke the setup controller. Usually `true` is the better option.
     */
    private boolean autoCreateSignatoryDid = true;

    /**
     * The time in seconds between the retries of asking the authority for the signed SelfDescription. No effect if scheduling not configured.
     */
    private int selfDescriptionRefreshRate = 10;

    /**
     * The amount of time a self-description is valid
     */
    private Duration selfDescriptionValidityDuration = Duration.ofDays(30);

    /**
     * The amount of time the remote contexts should be cached to avoid blocking
     */
    private Duration contextDocumentCacheDuration = Duration.ofDays(1);

    /**
     * The time in seconds between refreshes of provided catalogue feature. Needs scheduling to be enabled.
     */
    private int catalogueRefreshRate = 60;

    /**
     * Add @see org.atlasx.policies.AtlasSignaturePolicy to all protected paths
     */
    private boolean policySignatureEnabled = true;

    /**
     * Add @see org.atlasx.policies.AtlasPresentationCredentialsSignedPolicy to all protected paths
     */
    private boolean policySignedPresentationCredentialsEnabled = true;

    /**
     * Add @see id.walt.auditor.policies.IssuedDateBeforePolicy to all protected paths
     */
    private boolean policyIssuedDateEnabled = true;

    /**
     * Add @see id.walt.auditor.policies.ValidFromBeforePolicy to all protected paths
     */
    private boolean policyValidFromEnabled = true;

    /**
     * Add @see id.walt.auditor.policies.ExpirationDateAfterPolicy to all protected paths
     */
    private boolean policyExpirationEnabled = true;

    /**
     * Configure the used method within the initializer
     */
    private DidMethod defaultDidMethod = DidMethod.web;
}
