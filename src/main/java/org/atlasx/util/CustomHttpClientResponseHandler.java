package org.atlasx.util;

import org.apache.hc.core5.http.ClassicHttpResponse;
import org.apache.hc.core5.http.EntityDetails;
import org.apache.hc.core5.http.HttpEntity;
import org.apache.hc.core5.http.io.HttpClientResponseHandler;

import java.io.IOException;

public class CustomHttpClientResponseHandler implements HttpClientResponseHandler<CustomHttpClientResponseHandler.Response> {
    @Override
    public Response handleResponse(ClassicHttpResponse response) {
        int code = response.getCode();
        try {
            HttpEntity entity = response.getEntity();
            byte[] bytes = entity.getContent().readAllBytes();
            return new Response(code, bytes, entity, null);
        } catch (IOException e) {
            return new Response(code, null, null, e);
        }
    }

    public record Response (
            int code,
            byte[] data,
            EntityDetails details,
            Exception e
    ) {}
}
