package org.atlasx.util;

import id.walt.credentials.w3c.VerifiableCredential;
import id.walt.credentials.w3c.VerifiablePresentation;
import org.jetbrains.annotations.NotNull;

public class CredentialUtil {
    public static VerifiableCredential fromString(@NotNull String auth) {
        String vcString = auth.trim().startsWith("Bearer") ? auth.trim().substring("Bearer".length()).trim() : auth.trim();
        VerifiableCredential authVc = VerifiableCredential.Companion.fromString(vcString);
        if (authVc.getType().contains("VerifiablePresentation")) {
            authVc = VerifiablePresentation.Companion.fromString(vcString);
        }
        return authVc;
    }
}
