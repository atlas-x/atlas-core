package org.atlasx.util;

public interface AtlasEventPublisher {
    void publish(Object event);
}
