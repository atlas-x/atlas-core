package org.atlasx.policies;

import id.walt.auditor.SimpleVerificationPolicy;
import id.walt.auditor.VerificationPolicyResult;
import id.walt.credentials.w3c.VerifiableCredential;
import org.jetbrains.annotations.NotNull;

public class GxCredentialSignedByAuthorityVerificationPolicy extends SimpleVerificationPolicy {
    @NotNull
    @Override
    public String getDescription() {
        return "bla bla bla";
    }

    @NotNull
    @Override
    protected VerificationPolicyResult doVerify(@NotNull VerifiableCredential vc) {
        if (!vc.getType().contains("GaiaxCredential")) {
            return VerificationPolicyResult.Companion.success();
        }
        boolean valid = vc.getProof().getCreator().startsWith("did:web:authority.atlas.cartrust.com:gx:did:");
        // check if proof was made by authority
        return valid ? VerificationPolicyResult.Companion.success() : VerificationPolicyResult.Companion.failure(new Exception("GaiaxCredential needs to be proofed by carTRUST authority"));
    }
}
