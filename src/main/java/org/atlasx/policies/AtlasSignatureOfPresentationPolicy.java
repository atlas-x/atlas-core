package org.atlasx.policies;

import id.walt.services.vc.WaltIdJsonLdCredentialService;
import id.walt.services.vc.WaltIdJwtCredentialService;

public class AtlasSignatureOfPresentationPolicy extends AtlasSignaturePolicy {
    public AtlasSignatureOfPresentationPolicy(
            WaltIdJsonLdCredentialService jsonLdCredentialService,
            WaltIdJwtCredentialService jwtCredentialService) {
        super(jsonLdCredentialService, jwtCredentialService);
    }

    public AtlasSignatureOfPresentationPolicy(AtlasSignaturePolicy parent) {
        super(parent.jsonLdCredentialService, parent.jwtCredentialService);
    }

    @Override
    public boolean getApplyToVC() {
        return false;
    }
}
