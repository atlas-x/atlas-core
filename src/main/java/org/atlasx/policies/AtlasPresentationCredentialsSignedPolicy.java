package org.atlasx.policies;

import id.walt.auditor.VerificationPolicy;
import id.walt.auditor.VerificationPolicyResult;
import id.walt.credentials.w3c.VerifiableCredential;
import id.walt.credentials.w3c.VerifiablePresentation;
import id.walt.services.vc.VerificationResult;
import id.walt.services.vc.WaltIdJsonLdCredentialService;
import id.walt.services.vc.WaltIdJwtCredentialService;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

@Slf4j
public class AtlasPresentationCredentialsSignedPolicy extends VerificationPolicy {
    protected final WaltIdJsonLdCredentialService jsonLdCredentialService;
    protected final WaltIdJwtCredentialService jwtCredentialService;

    @Override
    public boolean getApplyToVC() {
        return false; // Not for VCs
    }

    @Override
    public boolean getApplyToVP() {
        return true;
    }

    public AtlasPresentationCredentialsSignedPolicy(
            WaltIdJsonLdCredentialService jsonLdCredentialService,
            WaltIdJwtCredentialService jwtCredentialService
    ) {
        this.jsonLdCredentialService = jsonLdCredentialService;
        this.jwtCredentialService = jwtCredentialService;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Checks if all presented credentials are signed.";
    }

    @NotNull
    @Override
    protected VerificationPolicyResult doVerify(@NotNull VerifiableCredential verifiableCredential) {
        if (verifiableCredential instanceof VerifiablePresentation vp) {
            if (vp.getVerifiableCredential() == null) {
                return VerificationPolicyResult.Companion.failure(new Exception("Verifiable presentation must contain least one VC"));
            }
            Exception[] exceptions = vp.getVerifiableCredential()
                    .stream()
                    .map(this::verifyByFormatType)
                    .filter(VerificationResultPair::failure)
                    .map(p -> {
                        String types = String.join(",", p.vc.getType());
                        return new Exception("[" + types + "]: " + p.result.toString());
                    })
                    .toArray(Exception[]::new);
            VerificationPolicyResult.Companion.failure(exceptions);
        }
        return VerificationPolicyResult.Companion.success();
    }

    private VerificationResultPair verifyByFormatType(VerifiableCredential vc) {
        String vcString = vc.encode();
        return new VerificationResultPair(vc, Optional.ofNullable(vc.getSdJwt())
                .map(n -> jwtCredentialService.verify(vcString))
                .orElseGet(() -> jsonLdCredentialService.verify(vcString)));
    }

    record VerificationResultPair(VerifiableCredential vc, VerificationResult result) {
        boolean failure() {
            return !result.getVerified();
        }
    }
}
