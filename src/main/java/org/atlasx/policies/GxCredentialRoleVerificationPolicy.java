package org.atlasx.policies;

import id.walt.auditor.SimpleVerificationPolicy;
import id.walt.auditor.VerificationPolicyResult;
import id.walt.credentials.w3c.VerifiableCredential;
import id.walt.credentials.w3c.W3CCredentialSubject;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Map;

public class GxCredentialRoleVerificationPolicy extends SimpleVerificationPolicy {
    private final List<String> roles;

    public GxCredentialRoleVerificationPolicy(List<String> roles) {
        this.roles = roles;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Verify roles to be present";
    }

    @NotNull
    @Override
    protected VerificationPolicyResult doVerify(@NotNull VerifiableCredential vc) {
        if (!vc.getType().contains("GaiaxCredential")) {
            return VerificationPolicyResult.Companion.success();
        }
        if (roles == null || roles.isEmpty()) {
            return VerificationPolicyResult.Companion.success();
        }
        W3CCredentialSubject subject = vc.getCredentialSubject();
        Object role = null;
        if(subject.getProperties().get("properties") instanceof Map<?, ?>) {
            Map<?, ?> properties = (Map<?, ?>)subject.getProperties().get("properties");
            role = properties.get("role");
            if (role != null && (role instanceof List<?>)) {
                List<?> vcRoles = (List<?>) role;
                boolean requiredRolesPresent = roles.stream().allMatch(vcRoles::contains);
                if (!requiredRolesPresent) {
                    return VerificationPolicyResult.Companion.failure(new Exception("Not all required roles found in SD"));
                }
                return VerificationPolicyResult.Companion.success();
            }
        }
        return VerificationPolicyResult.Companion.failure(new Exception("No roles provided in SD"));
    }
}
