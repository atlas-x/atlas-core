package org.atlasx.policies;

import id.walt.auditor.VerificationPolicy;
import id.walt.auditor.VerificationPolicyResult;
import id.walt.credentials.w3c.VerifiableCredential;
import id.walt.services.vc.VerificationResult;
import id.walt.services.vc.WaltIdJsonLdCredentialService;
import id.walt.services.vc.WaltIdJwtCredentialService;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

@Slf4j
public class AtlasSignaturePolicy extends VerificationPolicy {
    protected final WaltIdJsonLdCredentialService jsonLdCredentialService;
    protected final WaltIdJwtCredentialService jwtCredentialService;

    public AtlasSignaturePolicy(
            WaltIdJsonLdCredentialService jsonLdCredentialService,
            WaltIdJwtCredentialService jwtCredentialService) {
        this.jsonLdCredentialService = jsonLdCredentialService;
        this.jwtCredentialService = jwtCredentialService;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Verify signature of VC and VP. Containing VCs of VP are not verified.";
    }

    @NotNull
    @Override
    protected VerificationPolicyResult doVerify(@NotNull VerifiableCredential vc) {
        try {
            log.debug("is jwt: {}", vc.getSdJwt() != null);
            VerificationResult result = verifyByFormatType(vc);
            return result.getVerified()
                    ? VerificationPolicyResult.Companion.success()
                    : VerificationPolicyResult.Companion.failure(new Exception(result.toString()));
        } catch (Throwable e) {
            log.debug("Signature policy check failed due to exception", e);
            return VerificationPolicyResult.Companion.failure(e);
        }
    }

    private VerificationResult verifyByFormatType(VerifiableCredential vc) {
        String vcString = vc.encode();
        return Optional.ofNullable(vc.getSdJwt())
                .map(n -> jwtCredentialService.verify(vcString))
                .orElseGet(() -> jsonLdCredentialService.verify(vcString));
    }
}
