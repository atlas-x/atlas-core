package org.atlasx.policies;

import id.walt.auditor.VerificationPolicy;
import id.walt.auditor.VerificationPolicyResult;
import id.walt.credentials.w3c.VerifiableCredential;
import id.walt.credentials.w3c.VerifiablePresentation;
import id.walt.services.vc.WaltIdJsonLdCredentialService;
import org.jetbrains.annotations.NotNull;

import java.net.URI;

public class AtlasVCSchemaPolicy extends VerificationPolicy {
    private final WaltIdJsonLdCredentialService jsonLdCredentialService;

    public AtlasVCSchemaPolicy(WaltIdJsonLdCredentialService jsonLdCredentialService) {
        this.jsonLdCredentialService = jsonLdCredentialService;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Policy to check JSON-LD schema";
    }

    @NotNull
    @Override
    protected VerificationPolicyResult doVerify(@NotNull VerifiableCredential verifiableCredential) {
        if (verifiableCredential instanceof VerifiablePresentation) {
            VerificationPolicyResult.Companion.success();
        }
        if (verifiableCredential.getSdJwt() != null) {
            return VerificationPolicyResult.Companion.success();
        }
        verifiableCredential.getContext().get(0).getUri();

        return verifiableCredential.getContext()
                .stream()
                .map(ctx -> {
                    String uri = ctx.getUri();
                    URI schemaURI = URI.create(uri);
                    return jsonLdCredentialService.validateSchema(verifiableCredential, schemaURI);
                })
                .filter(pr -> pr.isFailure())
                .findFirst()
                .orElse(VerificationPolicyResult.Companion.success());
    }
}
