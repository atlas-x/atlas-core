package org.atlasx.policies;

import id.walt.auditor.VerificationPolicy;
import id.walt.auditor.VerificationPolicyResult;
import id.walt.credentials.w3c.VerifiableCredential;
import id.walt.credentials.w3c.VerifiablePresentation;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

public class PresentedBySubjectVerificationPolicy extends VerificationPolicy {
    @Override
    public boolean getApplyToVC() {
        return false;
    }

    @Override
    public boolean getApplyToVP() {
        return true;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Presenter did is subject id of all credentials";
    }

    @NotNull
    @Override
    protected VerificationPolicyResult doVerify(@NotNull VerifiableCredential verifiableCredential) {
        if (!(verifiableCredential instanceof VerifiablePresentation vp)) {
            return VerificationPolicyResult.Companion.success();
        }
        String presentationCreator = vp.getIssuerId(); // This should be the holder of all VCs
        boolean success = Objects.requireNonNull(vp.getVerifiableCredential())
                .stream()
                .allMatch(vc -> Objects.equals(presentationCreator, vc.getSubjectId()));
        if (success) {
            return VerificationPolicyResult.Companion.success();
        }
        return VerificationPolicyResult.Companion.failure(new Exception("All presented VCs must have the presenter as subject"));
    }
}
