package org.atlasx.catalogue;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.walt.credentials.w3c.VerifiableCredential;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.hc.core5.http.ContentType;
import org.apache.hc.core5.http.Method;
import org.apache.hc.core5.http.io.support.ClassicRequestBuilder;
import org.atlasx.AtlasCommunicator;
import org.atlasx.config.AtlasConfigProperties;
import org.atlasx.gx.GxServiceOffering;
import org.atlasx.gx.ServiceOfferingUtils;

import java.net.URI;
import java.util.List;

@Slf4j
public class AtlasCataloguePublisher {
    private final AtlasCatalogue catalogue;
    private final AtlasConfigProperties configProperties;
    private final AtlasCommunicator communicator;
    private final ObjectMapper objectMapper;

    @Getter
    private boolean loaded = false;

    public AtlasCataloguePublisher(
            AtlasCatalogue catalogue,
            AtlasConfigProperties configProperties,
            AtlasCommunicator communicator,
            ObjectMapper objectMapper) {
        this.catalogue = catalogue;
        this.configProperties = configProperties;
        this.communicator = communicator;
        this.objectMapper = objectMapper;
    }



    public void init() {
        String myCatalogueAddress = configProperties.getServiceBaseAddress() + "/gx/catalogue";
        List<VerifiableCredential> offers = this.catalogue.find("ServiceOfferingCatalogue");
        log.info("Found {} for ServiceOfferingCatalogue", offers.size());
        offers.stream().findFirst().ifPresent(offer -> {
            try {
                String webAddress = getWebAddress(offer);
                log.info("Register '{}' at '{}'", myCatalogueAddress, webAddress);
                URI uri = URI.create(webAddress);
                ClassicRequestBuilder requestBuilder = communicator.requestBuilder(Method.POST, ContentType.TEXT_PLAIN, uri);
                requestBuilder.setEntity(myCatalogueAddress);
                communicator.exchange(requestBuilder);
                loaded = true;
            } catch (Exception e) {
                log.info("Exception while communicating", e);
            }
        });
    }

    private String getWebAddress(VerifiableCredential vc) throws JsonProcessingException {
        GxServiceOffering subject = objectMapper.readValue(vc.getCredentialSubject().toJson(), GxServiceOffering.class);

        String endpoint = ServiceOfferingUtils.resolveEndpointAddress(subject)
                .orElseThrow( () -> new IllegalStateException("ServiceOffering does not provide endpoint"));
        return endpoint + "/catalogue";
    }

}
