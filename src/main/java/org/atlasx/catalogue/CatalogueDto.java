package org.atlasx.catalogue;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CatalogueDto {
    @Singular
    private List<String> peers;
    @Singular
    private List<Object> offerings;
}
