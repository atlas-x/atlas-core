package org.atlasx.catalogue;

import lombok.*;

import java.util.List;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AtlasCatalogueConfiguration {
    @Singular
    private List<String> peers;
}
