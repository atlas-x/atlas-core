package org.atlasx.catalogue;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.walt.credentials.w3c.VerifiableCredential;
import lombok.extern.slf4j.Slf4j;
import org.apache.hc.client5.http.HttpResponseException;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.impl.classic.BasicHttpClientResponseHandler;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.core5.http.*;
import org.apache.hc.core5.http.io.HttpClientResponseHandler;
import org.apache.hc.core5.http.io.entity.EntityUtils;
import org.apache.hc.core5.net.URIBuilder;
import org.atlasx.config.AtlasInitializer;
import org.atlasx.gx.AtlasServiceOfferingManager;
import org.atlasx.util.AtlasEventPublisher;
import org.atlasx.util.CustomHttpClientResponseHandler;

import java.io.IOException;
import java.net.URI;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
public class AtlasCatalogue {
    private final AtlasCatalogueConfiguration atlasCatalogueConfiguration;
    private final AtlasInitializer atlasInitializer;
    private final ObjectMapper objectMapper;
    private final AtlasEventPublisher eventPublisher;
    private final AtlasServiceOfferingManager atlasServiceOfferingManager;
    private List<String> additionalPeers;
    private boolean initialized;
    private Collection<VerifiableCredential> data;

    public AtlasCatalogue(
            AtlasCatalogueConfiguration atlasCatalogueConfiguration,
            AtlasInitializer atlasInitializer,
            ObjectMapper objectMapper,
            AtlasEventPublisher eventPublisher,
            AtlasServiceOfferingManager atlasServiceOfferingManager) {
        this.atlasCatalogueConfiguration = atlasCatalogueConfiguration;
        this.atlasInitializer = atlasInitializer;
        this.objectMapper = objectMapper;
        this.eventPublisher = eventPublisher;
        this.atlasServiceOfferingManager = atlasServiceOfferingManager;
        additionalPeers = new ArrayList<>();
    }

    public synchronized void init() {
        if (initialized) {
            return;
        }
        this.initialized = true;
        load();
    }

    public List<VerifiableCredential> find(String... name) {
        if (name == null || data == null) {
            return List.of();
        }
        Collection<VerifiableCredential> ref = data;
        Set<String> names = Set.of(name);
        return ref
                .stream()
                .filter(vc -> vc
                        .getType()
                        .stream()
                        .anyMatch(names::contains))
                .toList();
    }

    public List<VerifiableCredential> findById(String... id) {
        if (id == null || data == null) {
            return List.of();
        }
        Collection<VerifiableCredential> ref = data;
        Set<String> ids = Set.of(id);
        return ref
                .stream()
                .filter(vc -> ids.contains(vc.getId()))
                .toList();
    }

    public void load() {
        if (!atlasInitializer.isInitialized()) {
            log.debug("Atlas not initialized - please invoke setup identity api");
            return;
        }
        Map<String, VerifiableCredential> myServiceOfferings = Optional.ofNullable(atlasServiceOfferingManager)
                .map(AtlasServiceOfferingManager::getAll)
                .orElse(new HashMap<>());
        List<CatalogueDto> catalogueDtos = peers()
                .stream()
                .map(this::queryPeer)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .toList();

        Map<String, VerifiableCredential> all = myServiceOfferings.values().stream().collect(Collectors.toMap(VerifiableCredential::getId, e -> e));

        List<VerifiableCredential> remoteOfferings = catalogueDtos.stream()
                .flatMap(c -> c.getOfferings().stream())
                .map(this::writeSilent)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(this::fromString).toList();

        remoteOfferings.forEach(o -> all.computeIfAbsent(o.getId(), (id) -> o));
        Collection<VerifiableCredential> values = all.values();
        data = new ArrayList<>(values);
        log.info("Loaded - emitting event");
        eventPublisher.publish(new AtlasCatalogueLoadedEvent());
    }

    public List<String> peers() {
        List<String> buffer = new ArrayList<>();
        buffer.addAll(atlasCatalogueConfiguration.getPeers());
        buffer.addAll(additionalPeers);
        return Collections.unmodifiableList(buffer);
    }

    public boolean registerPeer(String address) {
        List<String> peers = peers();
        if (peers.contains(address)) {
            return false;
        }
        List<String> buffer = new ArrayList<>(peers);
        buffer.add(address);
        additionalPeers = buffer;
        load();
        return true;
    }

    public CatalogueDto present() {
        CatalogueDto.CatalogueDtoBuilder builder = CatalogueDto.builder()
                .peers(peers());

        if (data != null) {
            data.stream()
                    .map(VerifiableCredential::encode)
                    .map(this::readSilent)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .forEach(builder::offering);
        }

        return builder.build();
    }

    private Optional<CatalogueDto> queryPeer(String peer) {
        URI uri;
        try {
            URIBuilder uriBuilder = new URIBuilder(peer);
            uri = uriBuilder.build();
        } catch (Exception e) {
            log.error(e.getMessage());
            return Optional.empty();
        }

        HttpGet httpGet = new HttpGet(uri);
        httpGet.setHeader(HttpHeaders.ACCEPT, ContentType.APPLICATION_JSON);


        HttpClientResponseHandler<String> responseHandler = new HttpClientResponseHandler<String>() {

            @Override
            public String handleResponse(ClassicHttpResponse response) throws HttpException, IOException {
                return EntityUtils.toString(response.getEntity());
            }
        };

        try (
                CloseableHttpClient client = HttpClients.createDefault();
        ) {
            String body = client.execute(httpGet, responseHandler);
            return Optional.ofNullable(objectMapper.readValue(body, CatalogueDto.class));
        } catch (Exception e) {
            log.info(e.getMessage());
        }
        return Optional.empty();
    }

    private Optional<String> writeSilent(Object o) {
        try {
            return Optional.ofNullable(objectMapper.writeValueAsString(o));
        } catch (JsonProcessingException e) {
            log.debug("JsonProcessingException: {}", e.getMessage());
            return Optional.empty();
        }
    }

    private Optional<Object> readSilent(String s) {
        try {
            return Optional.ofNullable(objectMapper.readValue(s, Object.class));
        } catch (JsonProcessingException e) {
            return Optional.empty();
        }
    }

    private VerifiableCredential fromString(String s) {
        return VerifiableCredential.Companion.fromString(s);
    }
}
