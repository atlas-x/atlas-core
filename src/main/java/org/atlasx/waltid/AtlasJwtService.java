package org.atlasx.waltid;

import id.walt.sdjwt.JwtVerificationResult;
import id.walt.services.jwt.WaltIdJwtService;
import id.walt.services.key.KeyService;
import org.jetbrains.annotations.NotNull;

public class AtlasJwtService extends WaltIdJwtService {
    private final KeyService keyService;

    public AtlasJwtService(KeyService keyService) {
        this.keyService = keyService;
    }

    @NotNull
    @Override
    public KeyService getKeyService() {
        return keyService;
    }

    @Override
    public JwtVerificationResult verify(String token) {
        return super.verify(token);
    }
}
