package org.atlasx.waltid;

import id.walt.services.crypto.CryptoService;
import id.walt.services.key.WaltIdKeyService;
import id.walt.services.keystore.KeyStoreService;
import org.jetbrains.annotations.NotNull;

public class AtlasKeyService extends WaltIdKeyService {
    private final CryptoService cryptoService;

    private final AtlasKeystoreService keystoreService;

    public AtlasKeyService(AtlasKeystoreService keystoreService, AtlasCryptoService cryptoService) {
        this.keystoreService = keystoreService;
        this.cryptoService = cryptoService;
    }

    @NotNull
    @Override
    public CryptoService getCryptoService() {
        return cryptoService;
    }

    @NotNull
    @Override
    public KeyStoreService getKeyStore() {
        return keystoreService;
    }
}
