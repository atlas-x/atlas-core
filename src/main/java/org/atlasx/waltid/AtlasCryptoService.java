package org.atlasx.waltid;

import id.walt.crypto.KeyAlgorithm;
import id.walt.crypto.KeyId;
import id.walt.services.crypto.CryptoService;
import id.walt.services.crypto.SunCryptoService;
import org.jetbrains.annotations.NotNull;
import org.web3j.crypto.ECDSASignature;

public class AtlasCryptoService extends CryptoService {
    private final SunCryptoService cryptoService;

    public AtlasCryptoService(AtlasKeystoreService keystoreService) {
        SunCryptoService sunCryptoService = new SunCryptoService();
        sunCryptoService.setKeyStore(keystoreService);
        this.cryptoService = sunCryptoService;
    }

    @NotNull
    @Override
    public KeyId generateKey(@NotNull KeyAlgorithm algorithm)  {
        return cryptoService.generateKey(algorithm);
    }


    @NotNull
    @Override
    public byte[] sign(@NotNull KeyId keyId, @NotNull byte[] data) {
        return cryptoService.sign(keyId, data);
    }


    @Override
    public boolean verify(@NotNull KeyId keyId, @NotNull byte[] sig, @NotNull byte[] data) {
        return cryptoService.verify(keyId, sig, data);
    }

    
    @NotNull
    @Override
    public byte[] encrypt(
            @NotNull KeyId keyId,
            @NotNull String algorithm,
            @NotNull byte[] plainText,
            byte[] authData,
            byte[] iv
    ) {
        return cryptoService.encrypt(keyId, algorithm, plainText, authData, iv);
    }

    @NotNull
    @Override
    public byte[] decrypt(
            @NotNull KeyId keyId,
            @NotNull String algorithm,
            @NotNull byte[] plainText,
            byte[] authData,
            byte[] iv
    ) {
        return cryptoService.decrypt(keyId, algorithm, plainText, authData, iv);
    }

    @NotNull
    @Override
    public ECDSASignature signEthTransaction(@NotNull KeyId keyId, @NotNull byte[] encodedTx) {
        return cryptoService.signEthTransaction(keyId, encodedTx);

    }
}
