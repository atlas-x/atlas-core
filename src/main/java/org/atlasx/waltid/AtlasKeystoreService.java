package org.atlasx.waltid;

import id.walt.crypto.Key;
import id.walt.crypto.KeyId;
import id.walt.services.hkvstore.HKVStoreService;
import id.walt.services.keystore.HKVKeyStoreService;
import id.walt.services.keystore.KeyType;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class AtlasKeystoreService extends HKVKeyStoreService {
    private final HKVStoreService hkvStoreService;

    public AtlasKeystoreService(HKVStoreService hkvStoreService) {
        this.hkvStoreService = hkvStoreService;
    }

    @NotNull
    @Override
    public HKVStoreService getHkvStore() {
        return hkvStoreService;
    }

    @Override
    public void store(@NotNull Key key) {
        super.store(key);
    }

    @NotNull
    @Override
    public Key load(@NotNull String alias, @NotNull KeyType keyType)  {
        return super.load(alias, keyType);
    }

    @Override
    public String getKeyId(@NotNull String alias) {
        return super.getKeyId(alias);
    }

    @Override
    public void addAlias(@NotNull KeyId keyId, @NotNull String alias) {
        super.addAlias(keyId, alias);
    }

    @NotNull
    @Override
    public List<Key> listKeys() {
        return super.listKeys();
    }
}
