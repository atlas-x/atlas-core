package org.atlasx.waltid;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class S3HKVStoreProperties {
    private String bucket;
    private String endpoint;
    private String region;
}
