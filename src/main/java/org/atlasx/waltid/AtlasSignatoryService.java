package org.atlasx.waltid;

import id.walt.credentials.w3c.VerifiableCredential;
import id.walt.credentials.w3c.W3CIssuer;
import id.walt.credentials.w3c.builder.AbstractW3CCredentialBuilder;
import id.walt.credentials.w3c.templates.VcTemplate;
import id.walt.model.VerificationMethod;
import id.walt.signatory.ProofConfig;
import id.walt.signatory.Signatory;
import id.walt.signatory.SignatoryDataProvider;
import id.walt.signatory.WaltIdSignatory;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.atlasx.config.AtlasConfigProperties;
import org.atlasx.config.AtlasInitializer;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

@Slf4j
public class AtlasSignatoryService extends Signatory {
    private WaltIdSignatory walt;
    private final AtlasConfigProperties configuration;
    private final AtlasInitializer initializer;

    public AtlasSignatoryService(AtlasConfigProperties configuration, AtlasInitializer initializer) {
        this.configuration = configuration;
        this.initializer = initializer;
    }

    @SneakyThrows
    private WaltIdSignatory getWalt() {
        if (!initializer.isInitialized()) {
            throw new RuntimeException("Signatory needs initialized atlas");
        }
        if (walt != null) {
            return walt;
        }

        String issuerDid = initializer.getIssuerDidDocument().getId();
        VerificationMethod verificationMethod = initializer.getIssuerVerificationMethod();
        String domain = configuration.getDomain();
        String nonce = configuration.getNonce();

        String data = "proofConfig {\n" +
                "    issuerDid=\"" + issuerDid +"\"\n" +
                "    issuerVerificationMethod=\"" + verificationMethod.getId() +"\"\n" +
                "    proofType=\"JWT\"\n" +
                "    domain=\"" + domain + "\"\n" +
                "    nonce=\"" + nonce + "\"\n" +
                "}";

        File tempFile = File.createTempFile("WaltIdSignatory", ".conf");
        Files.writeString(Paths.get(tempFile.toURI()), data);

        this.walt = new WaltIdSignatory(tempFile.getAbsolutePath());

        return walt;
    }

    @Override
    public boolean hasTemplateId(@NotNull String templateId) {
        return getWalt().hasTemplateId(templateId);
    }

    @Override
    public void importTemplate(@NotNull String templateId, @NotNull String template) {
        getWalt().importTemplate(templateId, template);
    }

    @NotNull
    @Override
    public String issue(@NotNull AbstractW3CCredentialBuilder<?, ?> credentialBuilder, @NotNull ProofConfig config, @Nullable W3CIssuer issuer, boolean storeCredential) {
        return getWalt().issue(credentialBuilder, config, issuer, storeCredential);
    }

    @NotNull
    @Override
    public String issue(@NotNull String templateIdOrFilename, @NotNull ProofConfig config, @Nullable SignatoryDataProvider dataProvider, @Nullable W3CIssuer issuer, boolean storeCredential) {
        return getWalt().issue(templateIdOrFilename, config, dataProvider, issuer, storeCredential);
    }

    @NotNull
    @Override
    public List<String> listTemplateIds() {
        return getWalt().listTemplateIds();
    }

    @NotNull
    @Override
    public List<VcTemplate> listTemplates() {
        return getWalt().listTemplates();
    }

    @NotNull
    @Override
    public VerifiableCredential loadTemplate(@NotNull String templateId) {
        return getWalt().loadTemplate(templateId);
    }

    @Override
    public void removeTemplate(@NotNull String templateId) {
        getWalt().removeTemplate(templateId);
    }

    //    @NotNull
//    @Override
//    public SignatoryConfig getConfiguration() {
//        return new SignatoryConfig(
//                proofConfig,
//                configuration.getTemplatesFolder()
//        );
//    }

}
