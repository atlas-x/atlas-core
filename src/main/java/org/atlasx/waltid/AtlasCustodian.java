package org.atlasx.waltid;

import id.walt.credentials.w3c.PresentableCredential;
import id.walt.credentials.w3c.VerifiableCredential;
import id.walt.crypto.Key;
import id.walt.crypto.KeyAlgorithm;
import id.walt.crypto.KeyId;
import id.walt.custodian.Custodian;
import id.walt.services.key.KeyFormat;
import id.walt.services.key.KeyService;
import id.walt.services.keystore.KeyStoreService;
import id.walt.services.keystore.KeyType;
import id.walt.services.vc.JsonLdCredentialService;
import id.walt.services.vc.JwtCredentialService;
import id.walt.services.vcstore.VcStoreService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.time.Instant;
import java.util.List;

public class AtlasCustodian extends Custodian {
    private final KeyService keyService;
    private final JwtCredentialService jwtCredentialService;
    private final JsonLdCredentialService jsonLdCredentialService;
    private final KeyStoreService keyStore;
    private final VcStoreService vcStore;

    public AtlasCustodian(
            KeyService keyService,
            JwtCredentialService jwtCredentialService,
            JsonLdCredentialService jsonLdCredentialService,
            KeyStoreService keyStore,
            VcStoreService vcStore) {
        this.keyService = keyService;
        this.jwtCredentialService = jwtCredentialService;
        this.jsonLdCredentialService = jsonLdCredentialService;
        this.keyStore = keyStore;
        this.vcStore = vcStore;
    }

    public String getVcGroup() {
        return "custodian";
    }

    @NotNull
    @Override
    public Key generateKey(@NotNull KeyAlgorithm keyAlgorithm) {
        return keyStore.load(keyService.generate(keyAlgorithm).getId(), KeyType.PUBLIC);
    }

    @NotNull
    @Override
    public Key getKey(@NotNull String alias) {
        return keyStore.load(alias, KeyType.PUBLIC);
    }

    @NotNull
    @Override
    public List<Key> listKeys() {
        return keyStore.listKeys();
    }

    @NotNull
    @Override
    public KeyId importKey(@NotNull String keyStr) {
        return keyService.importKey(keyStr);
    }

    @Override
    public void deleteKey(@NotNull String id) {
        keyStore.delete(id);
    }

    @NotNull
    @Override
    public String exportKey(@NotNull String keyAlias, @NotNull KeyFormat format, @NotNull KeyType type) {
        return keyService.export(keyAlias, format, type);
    }

    @Nullable
    @Override
    public VerifiableCredential getCredential(@NotNull String id) {
        return vcStore.getCredential(id, getVcGroup());
    }

    @NotNull
    @Override
    public List<VerifiableCredential> listCredentials() {
        return vcStore.listCredentials(getVcGroup());
    }

    @NotNull
    @Override
    public List<String> listCredentialIds() {
        return vcStore.listCredentialIds(getVcGroup());
    }

    @Override
    public void storeCredential(@NotNull String alias, @NotNull VerifiableCredential vc) {
        vcStore.storeCredential(alias, vc, getVcGroup());
    }

    @Override
    public boolean deleteCredential(@NotNull String alias) {
        return vcStore.deleteCredential(alias, getVcGroup());
    }

    @NotNull
    @Override
    public String createPresentation(
            @NotNull List<PresentableCredential> vcs,
            @NotNull String holderDid,
            @Nullable String verifierDid,
            @Nullable String domain,
            @Nullable String challenge,
            @Nullable Instant expirationDate
    ) {
        if (vcs.stream().allMatch(PresentableCredential::isJwt)) {
            return jwtCredentialService.present(vcs, holderDid, verifierDid, challenge, expirationDate);
        }
        if (vcs.stream().noneMatch(PresentableCredential::isJwt)) {
            return jsonLdCredentialService.present(vcs, holderDid, verifierDid, challenge, expirationDate);
        }
        throw new IllegalStateException("All verifiable credentials must be of the same proof type.");
    }
}
