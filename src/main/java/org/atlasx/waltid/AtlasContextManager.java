package org.atlasx.waltid;

import id.walt.services.context.WaltIdContextManager;
import id.walt.services.hkvstore.HKVStoreService;
import id.walt.services.keystore.KeyStoreService;
import id.walt.services.vcstore.VcStoreService;
import org.jetbrains.annotations.NotNull;

public class AtlasContextManager extends WaltIdContextManager {
    private final HKVStoreService hkvStoreService;
    private final KeyStoreService keyStoreService;
    private final VcStoreService vcStoreService;

    public AtlasContextManager(HKVStoreService hkvStoreService, KeyStoreService keyStoreService, VcStoreService vcStoreService) {
        this.hkvStoreService = hkvStoreService;
        this.keyStoreService = keyStoreService;
        this.vcStoreService = vcStoreService;
    }

    @NotNull
    @Override
    public HKVStoreService getHkvStore() {
        return hkvStoreService;
    }

    @NotNull
    @Override
    public KeyStoreService getKeyStore() {
        return keyStoreService;
    }

    @NotNull
    @Override
    public VcStoreService getVcStore() {
        return vcStoreService;
    }
}
