package org.atlasx.waltid;

import com.github.benmanes.caffeine.cache.Cache;
import id.walt.credentials.w3c.templates.VcTemplateService;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Field;

@Slf4j
public class AtlasVcTemplateService extends VcTemplateService {
    @SneakyThrows
    public void invalidateCache() {
        Field templateCache = VcTemplateService.class.getDeclaredField("templateCache");
        templateCache.setAccessible(true);
        Cache<?,?> cache = (Cache<?,?>) templateCache.get(this);
        cache.invalidateAll();
    }
}
