package org.atlasx.waltid;

import id.walt.services.hkvstore.HKVKey;
import id.walt.services.hkvstore.HKVStoreService;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;
import software.amazon.awssdk.core.ResponseBytes;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.core.sync.ResponseTransformer;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.S3ClientBuilder;
import software.amazon.awssdk.services.s3.model.*;

import java.net.URI;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
public class S3HkvStore extends HKVStoreService {
    private final S3HKVStoreProperties properties;

    private final S3Client s3C;

    @SneakyThrows
    public S3HkvStore(S3HKVStoreProperties properties, @Nullable AwsCredentialsProvider awsCredentialsProvider) {
        this.properties = properties;
        S3ClientBuilder builder = S3Client.builder().region(Region.of(properties.getRegion()));
        if (StringUtils.isNotBlank(properties.getEndpoint())) {
            builder.endpointOverride(new URI(properties.getEndpoint()));
        }
        if (awsCredentialsProvider != null) {
            builder.credentialsProvider(awsCredentialsProvider);
        }
        s3C = builder.build();
    }

    public S3HkvStore(S3Client s3Client, S3HKVStoreProperties properties) {
        this.properties = properties;
        this.s3C = s3Client;
    }

    @SneakyThrows
    @Override
    public void put(@NotNull HKVKey key, @NotNull byte[] value) {
        PutObjectRequest request = PutObjectRequest.builder()
                .bucket(properties.getBucket())
                .key(key.toString())
                .build();

        s3C.putObject(request, RequestBody.fromBytes(value));
    }

    @SneakyThrows
    @Nullable
    @Override
    public byte[] getAsByteArray(@NotNull HKVKey key) {
        try {
            ResponseBytes<GetObjectResponse> r = s3C.getObject(GetObjectRequest.builder()
                            .bucket(properties.getBucket())
                            .key(key.toString())
                            .build(),
                    ResponseTransformer.toBytes());
            return r.asByteArray();
        } catch (Exception e) {
            log.debug("Failed to load {}", key, e);
            return null;
        }
    }

    @NotNull
    @Override
    public Set<HKVKey> listChildKeys(@NotNull HKVKey parent, boolean recursive) {

        ListObjectsRequest request = ListObjectsRequest.builder()
                .bucket(properties.getBucket())
                .prefix(parent +"/")
                .build();
        return s3C.listObjects(request)
                .contents()
                .stream()
                .map(o -> HKVKey.Companion.fromString(o.key()))
                .collect(Collectors.toSet());
    }

    @SneakyThrows
    @Override
    public boolean delete(@NotNull HKVKey key, boolean recursive) {
        DeleteObjectRequest deleteObjectRequest = DeleteObjectRequest.builder()
                .bucket(properties.getBucket())
                .key(key.toString())
                .build();

        s3C.deleteObject(deleteObjectRequest);
        return true;
    }
}
