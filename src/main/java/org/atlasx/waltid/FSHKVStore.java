package org.atlasx.waltid;

import id.walt.services.hkvstore.HKVKey;
import id.walt.services.hkvstore.HKVStoreService;
import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class FSHKVStore extends HKVStoreService {
    private final File root;

    public FSHKVStore(String path) {
        root = new File(path);
        if (!root.isDirectory()) {
            throw new RuntimeException("FS store path needs to be directory");
        }
        if (!root.canWrite()) {
            throw new RuntimeException("FS store path needs to be writeable");
        }
    }

    @Override
    public void put(@NotNull HKVKey key, @NotNull byte[] value) {
        Path combined = root.toPath().resolve(key.toPath());
        File parentFile = combined.getParent().toFile();
        if (!parentFile.exists() && !parentFile.mkdirs()) {
            throw new RuntimeException("Creating parent paths failed");
        }
        try {
            FileUtils.writeByteArrayToFile(combined.toFile(), value);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Nullable
    @Override
    public byte[] getAsByteArray(@NotNull HKVKey key) {
        Path combined = root.toPath().resolve(key.toPath());
        try {
            return FileUtils.readFileToByteArray(combined.toFile());
        } catch (IOException e) {
            return null;
        }
    }

    @NotNull
    @Override
    public Set<HKVKey> listChildKeys(@NotNull HKVKey parent, boolean recursive) {
        Path rootPath = root.toPath();
        File[] pathFileList = rootPath.resolve(parent.toPath()).toFile().listFiles();
        if (pathFileList == null) {
            return Set.of();
        }
        List<File> files = List.of(pathFileList);
        if (!recursive) {
            return files.stream()
                    .filter(File::isFile)
                    .map(File::toPath)
                    .map(rootPath::resolve)
                    .map(HKVKey.Companion::fromPath)
                    .collect(Collectors.toSet());
        }
        return files.stream()
                .map(f -> HKVKey.Companion.fromPath(rootPath.resolve(f.toPath())))
                .map(current -> {
                    if (current.toPath().toFile().isFile()) {
                        return List.of(current);
                    } else {
                        return listChildKeys(current, true);
                    }
                })
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());
    }

    @SneakyThrows
    @Override
    public boolean delete(@NotNull HKVKey key, boolean recursive) {
        Path combined = root.toPath().resolve(key.toPath());
        if (recursive) {
            FileUtils.deleteDirectory(combined.toFile());
            return true;
        }
        return combined.toFile().delete();
    }
}
