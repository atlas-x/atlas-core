package org.atlasx;

import id.walt.credentials.w3c.PresentableCredential;
import id.walt.credentials.w3c.VerifiableCredential;
import id.walt.services.vc.JwtCredentialService;
import id.walt.services.vcstore.VcStoreService;
import lombok.extern.slf4j.Slf4j;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.core5.http.*;
import org.apache.hc.core5.http.io.support.ClassicRequestBuilder;
import org.apache.hc.core5.http.message.BasicHeader;
import org.atlasx.config.AtlasConfigProperties;
import org.atlasx.config.AtlasInitializer;
import org.atlasx.util.AtlasEventPublisher;
import org.atlasx.util.CustomHttpClientResponseHandler;

import java.net.URI;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@Slf4j
public class AtlasCommunicator {
    private final AtlasEventPublisher atlasEventPublisher;
    private final AtlasConfigProperties configuration;
    private final VcStoreService vcStoreService;
    private final AtlasInitializer initializer;
    private final JwtCredentialService jwtCredentialService;

    public AtlasCommunicator(
            AtlasEventPublisher atlasEventPublisher,
            AtlasConfigProperties configuration,
            VcStoreService vcStoreService,
            AtlasInitializer initializer,
            JwtCredentialService jwtCredentialService
    ) {
        this.atlasEventPublisher = atlasEventPublisher;
        this.configuration = configuration;
        this.vcStoreService = vcStoreService;
        this.initializer = initializer;
        this.jwtCredentialService = jwtCredentialService;
    }

    public CustomHttpClientResponseHandler.Response exchange(ClassicRequestBuilder requestBuilder) {
        ClassicHttpRequest request = requestBuilder.build();
        String uri = request.getRequestUri();
        List<Header> authHeaders = Stream.of(request.getHeaders()).filter(h -> h.getName().equalsIgnoreCase(HttpHeaders.AUTHORIZATION)).toList();
        if (!authHeaders.isEmpty()) {
            String bearerPrefix = "Bearer ";
            authHeaders.stream()
                    .filter(bearer -> bearer.getValue().length() > bearerPrefix.length())
                    .map(bearer -> bearer.getValue().substring(bearerPrefix.length()))
                    .findFirst()
                    .ifPresent(token -> {
                        atlasEventPublisher.publish(new AtlasCommunicatorEvent(AtlasCommunicatorEvent.Type.BEARER, uri, token));
                    });
        }
        try (
                CloseableHttpClient client = HttpClients.createDefault();
        ) {
            return client.execute(request, new CustomHttpClientResponseHandler());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public ClassicRequestBuilder requestBuilder(
            Method method,
            ContentType contentType,
            URI uri,
            boolean authenticated) {
        if (authenticated) {
            return requestBuilder(method, contentType, uri);
        } else {
            return createRequestBuilder(method, contentType, uri);
        }
    }

    public ClassicRequestBuilder requestBuilder(
            Method method,
            ContentType contentType,
            URI uri,
            VerifiableCredential... vcs) {
        ClassicRequestBuilder request = createRequestBuilder(method, contentType, uri);

        Instant expiration = Instant.now().plus(5, ChronoUnit.MINUTES);
        String bearerAuth = createPresentation(expiration, vcs);
        log.debug("Add bearer auth: {}", bearerAuth);
        request.setHeader(new BasicHeader(HttpHeaders.AUTHORIZATION, "Bearer " + bearerAuth));

        return request;
    }

    private ClassicRequestBuilder createRequestBuilder(
            Method method,
            ContentType contentType,
            URI uri
    ) {
        ClassicRequestBuilder request = switch (method) {
            case GET -> ClassicRequestBuilder.get(uri);
            case HEAD -> ClassicRequestBuilder.head(uri);
            case POST -> ClassicRequestBuilder.post(uri);
            case PUT -> ClassicRequestBuilder.put(uri);
            case DELETE -> ClassicRequestBuilder.delete(uri);
            case CONNECT -> throw new RuntimeException("Unsupported method");
            case TRACE -> ClassicRequestBuilder.trace(uri);
            case OPTIONS -> ClassicRequestBuilder.options(uri);
            case PATCH -> ClassicRequestBuilder.patch(uri);
        };
        request.setHeaders(createHeaders(contentType));

        return request;
    }

    public Optional<VerifiableCredential> getSelfDescription() {
        String id = configuration.getSelfDescriptionVcId();
        String group = configuration.getVcGroup();
        return Optional.ofNullable(vcStoreService.getCredential(id, group));
    }

    private Header[] createHeaders(ContentType contentType) {
        return new Header[]{
                new BasicHeader(HttpHeaders.ACCEPT, ContentType.WILDCARD),
                new BasicHeader(HttpHeaders.CONTENT_TYPE, contentType),
        };
    }

    private String createPresentation(Instant expiration, VerifiableCredential... vcs) {
        String issuerDid = initializer.getIssuerDid();

        ArrayList<PresentableCredential> list = new ArrayList<>();
        getSelfDescription().map(this::toPresentable).ifPresent(list::add);

        Optional.ofNullable(vcs).ifPresent(ignore -> Arrays
                .stream(vcs)
                .map(this::toPresentable)
                .forEach(list::add));
        return jwtCredentialService.present(list, issuerDid, null, null, expiration);
    }

    private PresentableCredential toPresentable(VerifiableCredential vc) {
        return new PresentableCredential(vc, null, false);
    }
}
