package org.atlasx;

public record AtlasCommunicatorEvent(
        Type type, String remoteAddress, String token
) {
    public AtlasCommunicatorEvent(Type type, String token) {
        this(type, null, token);
    }

    public enum Type {
        BEARER
    }
}
