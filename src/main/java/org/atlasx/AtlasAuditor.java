package org.atlasx;

import id.walt.auditor.Auditor;
import id.walt.auditor.VerificationPolicy;
import id.walt.auditor.VerificationPolicyResult;
import id.walt.auditor.VerificationResult;
import id.walt.credentials.w3c.VerifiableCredential;
import id.walt.credentials.w3c.VerifiablePresentation;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toCollection;

@Slf4j
public class AtlasAuditor extends Auditor {

    @NotNull
    @Override
    public VerificationResult verify(@NotNull String vcJson, @NotNull List<? extends VerificationPolicy> policies) {
        VerifiableCredential verifiableCredential = toVerifiableCredential(vcJson);
        return this.verify(verifiableCredential, policies);
    }

    public static VerifiableCredential toVerifiableCredential(String input) {
        VerifiableCredential vc = VerifiableCredential.Companion.fromString(input);
        return vc.getType().contains("VerifiablePresentation")
                ? VerifiablePresentation.Companion.fromVerifiableCredential(vc)
                : vc;
    }

    @NotNull
    @Override
    public VerificationResult verify(@NotNull VerifiableCredential vc, @NotNull List<? extends VerificationPolicy> policies) {
        Comparator<VerificationPolicy> comparing = Comparator.comparing(VerificationPolicy::getId);

        ArrayList<? extends VerificationPolicy> uniquePolicies =
                policies.stream()
                        .collect(collectingAndThen(toCollection(() -> new TreeSet<>(comparing)), ArrayList::new));

        Map<String, VerificationPolicyResult> verificationPolicyResultMap =
                uniquePolicies.stream().collect(Collectors.toMap(VerificationPolicy::getId, policy -> {
                    log.debug("Verifying vc with {} ...", policy.getId());
                    VerificationPolicyResult vcResult = policy.verify(vc);
                    AtomicBoolean success = new AtomicBoolean(vcResult.isSuccess());
                    ArrayList<Throwable> allErrors = new ArrayList<>(vcResult.getErrors());
                    if (allErrors.isEmpty() && vc instanceof VerifiablePresentation) {
                        Optional.ofNullable(((VerifiablePresentation) vc).getVerifiableCredential())
                                .ifPresent(vp -> vp.forEach(cred -> {
                                    List<String> type = cred.getType();
                                    String lastTYpe = !type.isEmpty() ? type.get(type.size() - 1) : null;
                                    log.debug("Verifying {} in VP with {}...", lastTYpe, policy.getId());
                                    VerificationPolicyResult vpResult = policy.verify(cred);
                                    allErrors.addAll(vpResult.getErrors());
                                    success.compareAndSet(true, vpResult.isSuccess());
                                }));
                    }
                    allErrors.forEach(error -> log.error("{}: {}", policy.getId(), error.getMessage()));

                    if (success.get()) {
                        return VerificationPolicyResult.Companion.success();
                    }

                    return VerificationPolicyResult.Companion.failure(allErrors.toArray(new Throwable[0]));
                }));
        return new VerificationResult(allAccepted(verificationPolicyResultMap), verificationPolicyResultMap);
    }
}
