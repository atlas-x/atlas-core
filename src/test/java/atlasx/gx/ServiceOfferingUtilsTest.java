package atlasx.gx;

import org.atlasx.gx.GxServiceAccessPoint;
import org.atlasx.gx.GxServiceOffering;
import org.atlasx.gx.ServiceOfferingUtils;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

class ServiceOfferingUtilsTest {

    @Nested
    class ResolveEndpointAddress{
        @Test
        void thatItRetunsEmptyIfEndpointCannotBeResolved() {
            Optional<String> optionalEndpoint = ServiceOfferingUtils.resolveEndpointAddress(new GxServiceOffering());

            assertThat(optionalEndpoint).isNotPresent();
        }

        @Test
        void thatItResolvesServicePointsIfBothAreProvided() {
            GxServiceOffering serviceOfferingSubject = GxServiceOffering.builder()
                    .serviceAccessPoint(List.of(
                            GxServiceAccessPoint.builder()
                                    .host("example.org")
                                    .protocol("https")
                                    .build()
                    ))
                    .webAddress("http://example.com")
                    .build();
            Optional<String> optionalEndpoint = ServiceOfferingUtils.resolveEndpointAddress(serviceOfferingSubject);

            assertThat(optionalEndpoint).contains("https://example.org");
        }

        @Test
        void thatItResolvesWebAddressIfServicePointIsNotProvides() {
            GxServiceOffering serviceOfferingSubject = GxServiceOffering.builder()
                    .serviceAccessPoint(List.of())
                    .webAddress("http://example.com")
                    .build();
            Optional<String> optionalEndpoint = ServiceOfferingUtils.resolveEndpointAddress(serviceOfferingSubject);

            assertThat(optionalEndpoint).contains("http://example.com");
        }
    }


}